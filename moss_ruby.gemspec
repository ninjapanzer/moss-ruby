Gem::Specification.new do |s|
  s.name        = 'moss_ruby'
  s.version     = '1.1.2'
  s.date        = '2015-08-05'
  s.summary     = "Moss gem to access system for Detecting Software Plagiarism"
  s.description = "Moss-ruby is an unofficial ruby gem for the Moss system for Detecting Software Plagiarism (http://theory.stanford.edu/~aiken/moss/)"
  s.authors     = ["Andrew Cain"]
  s.email       = 'acain@swin.edu.au'
  s.files       = ["lib/moss_ruby.rb"]
  s.homepage    = 'https://bitbucket.org/macite/moss-ruby'
  s.license     = 'MIT'
end